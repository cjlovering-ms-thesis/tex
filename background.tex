\chapter{Background}\label{sec:background}

In this chapter we review techniques relevant to our approach. First, we describe the machine learning task of ranking. Ranking applies to our problem for a two different reasons. We find empirically that annotators have the highest agreement when labeling via pair-based comparisons \cref{sec:annotation}. Pairwise annotations allow us to either generate a ground-truth ranking use that to label our dataset. Alternatively, we can use the pairwise labels to train our models to directly learn a ranking function. We explore previous work in ranking to inform the tradeoffs of this decision. We also introduce current methods in natural language processing. Lastly, we define the notations used in this document.

\section{Ranking}\label{sec:background-ranking}

Ranking sorts items into a desired permutation. Thus, the goal is to find a function $h$ that is a bijective function that maps from $X \rightarrow X$ \cite{xia2008listwise}. However ranking functions usually optimize a function which operates on a single item \cite{xia2008listwise} for efficiency concerns. So, in order to find an ordinal relationship among items, a function $\psi : x_i \rightarrow \mathbb{R}$, is optimized such that if $\psi(x_i) > \psi(x_j)$ the rank of $x_i$ is greater than that of $x_j$ \cite{burges2005learning, chen2009ranking, xia2008listwise}. Items are then sorted using their corresponding scores. There are three different high-level approaches for training the ranking function $\psi$.

\subsection{Ranking Approaches}\label{sec:background-ranking-approaches}

\textbf{Pointwise models} operate on a single instance, and are reduced to regression or classification paradigms \cite{chen2009ranking}. MSE is a commonly employed loss (Eq. \ref{eq:mse}) where $y_i$ is either $\in [0, 1]$ or assigned to one of $\{0, 0.5, 1\}$ \cite{burges2005learning} and regress real valued scores $\in \mathbb{R}$. Other losses like the hinge loss and log loss \cite{chen2009ranking} are also common. In some cases, relevancy labels can be discrete like $\{0, 1, 2, \dots\}$ in which case cross entropy could optionally be used instead of MSE. In Eq. \ref{eq:mse}, $x$ is a set of items and $y$ is the set of labels (real-valued or discrete) for each of those items. Note that, as shown below in \cref{sec:background-ranking-evaluation}, ranking evaluation metrics use the raw values of the relevancy labels rather than viewing them as classes.

\begin{equation}\label{eq:mse}
    L(\psi; x, y) = \sum^n_{i=1} (\psi(x_i) - y_i)^2
\end{equation}

In our context, a pointwise model would operate on individual sentences. Candidate annotations for this type of approach include labeling each sentence as evidence or not, or optionally having different levels of relevance for each sentence, e.g. assigning a score $ \in \{0, 0.5, 1\}$ to each sentence. 

\textbf{Pairwise models} like RankSVM \cite{herbrich2000large}, RankBoost \cite{freund2003efficient}, and RankNet \cite{burges2005learning} are trained directly to order pairs. Burges \cite{burges2005learning} finds that the set of pairs do not need to be complete nor consistent to be effective. As noted by \cite{chen2009ranking}, the general form for pairwise loss functions is as follows:

\begin{align}\label{eq:pair}
  L(\psi; x, y) = \sum_{(i,j) : y_i > y_j} \phi(\psi(x_i) - \psi(x_j))
\end{align}

In Eq. \ref{eq:pair} $x$ is a set of items, and $y$ are the corresponding labels. Labels in $y$ can be a binary values, a discrete relevancy value like in the pointwise formulation, or the position in the desired permutation. This formulation also works for a set of pairwise comparison annotations. The specific function $\phi$ depends on the approach (e.g. RankSVM uses a hinge loss). There is significant research exploring these models: kernel methods for improving RankNet's accuracy \cite{kuo2014large} and techniques for increasing its scalability \cite{lee2014large} have been explored.

In the context of ER, each pair will be two candidate evidence sentences and the label will indicate which of the two sentences provide more evidence.

\textbf{Listwise models} also have been explored. Their loss function operates directly on the desired permutation. There are different formulations for listwise functions \cite{xia2008listwise, burges2010ranknet, burges2007learning, cao2007learning}. As an example, the loss function of ListMLE \cite{xia2008listwise} which penalizes items that have greater scores which are ranked lower than itself. However, even ListMLE's ranking function only operates on a single item. Each item in a sample (which is a set of items) is scored, and then the items are sorted into the output permutation. Because sorting is not differentiable, a likelihood loss is used as a ``surrogate'' as shown in Eq. \ref{eq:likelihood}. In Eq. \ref{eq:likelihood} $x$ is a sample of items to be ordered and $y$ is desired permutation.

\begin{align}\label{eq:likelihood}
 L(\psi, x, y) &= -\log P(y|x, \psi)\\
 P(y|x, \psi) &= \prod_{i=1}^n \frac{\exp(\psi(x_{y(i)})))} {\sum_{k=i}^n \exp(\psi(x_{y(k)})}
\end{align}

In summary, the integral ranking function $\psi$ for these different approaches operates on a single item and returns a real-valued score. This score is not typically probabilistic; its meaning is relative to other item scores.

\subsection{Ranking Evaluation}\label{sec:background-ranking-evaluation}

The evaluation metrics of ranking are used to measure how well the integral ranking function that scores items translates into the desired permutations of items. Here we describe Mean Average Precision, which is often used for ranking paradigms with pairwise or binary labels, and Normalized Discounted Cumulative Gain, which works for multiple levels of relevancy. 

For the following metrics we define $\pi_\psi$ as the permutation generated by the ranking function $\psi$; $\pi_\psi(x) = \text{argsort}([ \psi(x_i) | i = 1 .. |x| ])$.
Here, argsort returns the indices that would sort the scores. We also define $l$ to return the labels from a permutation $\pi$ as they index into $y$; $l(y, \pi) = [y_{\pi_i} | i = 1 .. |\pi| ]$. Indexing into $l(\pi)_i$ returns the relevancy of the the $i^{th}$ item in the ranking.

Mean Average Precision (MAP) is built on top of Precision@k \cite{baeza1999modern}. Precision@$k$ is the fraction of relevant documents found in top $k$ documents. MAP is Precision@k averaged for $k$ up to the number of relevant documents. This is defined for binary labels where relevant items ar have labels of 1 and other items have labels of 0. We define MAP in Eq. \ref{eq:map} as in \cite{chen2009ranking} where $n_1$ is the number of items (derived from $y$) that are equal to 1 and $I$ is the indicator function.

\begin{align}\label{eq:map}
    \text{MAP}(\psi, x, y) &= \frac{1}{n_1} \sum_{s=1}^{n_1} \text{Precision@$s$}(l(\pi_{\psi}(x)))\\ % =  \frac{1}{n_1} \sum_{s=1}^{n_1} \frac{\sum_{i \le s} I(_i)}{s}\\
    \text{Precision@$k$} (\pi) &= \frac{\sum_i^k I(\pi_i = 1)}{k}
\end{align}

Normalized Discounted Cumulative Gain (NDCG) \cite{wang2013theoretical} is a normalized measure that penalizes out-of-place items in a permutation. It is defined below in Eq. \ref{eq:ndcg}. It calculates the Discounted Cumulative Gain (DCG) which is a smoothed measure that penalizes highly ranked items ranked lowly. NCDG is normalized by the Ideal Discounted Cumulative Gain (IDCG) which is the maximal value of a DCG given a set of queries. DCG rewards ranking items with high relevancy scores higher, and discounts the scores of items lower in the ranking.

\begin{align}
    \text{NDCG}(\psi, x, y) &= \frac{\text{DCG}(\psi, x, y)}{\text{IDCG}(\psi, x, y)} \\
    \text{IDCG}(\psi, x, y) &= \max \text{DCG}(\psi, x, y) \\
    \text{DCG}(\psi, x, y) &= \sum_{i=1}^{n} G(l(\pi_\psi(x))_i) \times D(i)\\
    G(z) &= 2^z - 1 \\
    D(d) &= \frac{1}{\log_2(d + 1)} \\
\end{align}\label{eq:ndcg}

Note that $G(.)$ is an increasing function (i.e. gain) and that $D(.)$ is a decreasing function (i.e. discount) \cite{chen2009ranking}. In practice, people are often interested in NDCG@$k$ which is the NDCG up to an index $k$. In these cases, $D(d)$ can be set to zero when $d > k$. NDCG is discontinuous and thus not differentiable, so it is not commonly used directly for training models. However, it is found that losses on most pointwise and pairwise losses (e.g. cross entropy of pairwise scores) approach the behavior of NDCG \cite{chen2009ranking}.

\subsection{Generating Listwise Orderings from Pairwise Labels}

Instead of using the raw annotations we collect in \cref{sec:annotation}, we could preprocess these annotations into rankings. Wauthier et al. \cite{wauthier2013efficient} proposes a simple algorithm Balanced Rank Estimation which estimates the rank $\hat{\Pi}$ of an item $j$ among $n$ items as follows:

\begin{equation*}
\hat{\Pi}(j) = \frac{ \sum_{i \ne j} s_{i,j}(2 {c}_{i,j} - 1) }{p} \propto \sum_{i \ne j} s_{i,j}(2 {c}_{i,j} - 1)
\end{equation*}

where $s_{i,j}$ is the binary variable indicating whether or not ${c}_{i,j}$ was measured, ${c}_{i,j}$ is a binary variable indicating whether or not item $i$ has higher rank than $j$, and $p$ is the probability that the comparison $c_{i,j}$ was measured. We considered this technique as it could help us better utilize our pairwise annotations. The benefit of this approach is that the labels would be unified and consistent. However \cite{burges2005learning} finds that the set of pairs trained upon do not need to be complete nor consistent. So, we forgo using this technique for simplicity.

\section{Modeling Natural Language}\label{sec:background-modeling-natural-language}

Recent approaches to NLP use deep learning models like convolutional neural networks (CNN) \cite{kim2014convolutional} and recurrent networks such as long short-term memory (LSTM) \cite{hochreiter1997long} and the gated recurrent unit (GRU) \cite{chung2014empirical}. These deep learning methods have had marked success \cite{severyn2015learning} increasing previous benchmarks. Also they do not rely on external dependencies (e.g. part-of-speech annotations \cite{miller1995wordnet}), with the exception of optionally using word embeddings.

\subsection{Word Representations}

Word embeddings are representations of words in real-numbered vector space. These representations created by training the embeddings on large corpora of documents, and map individual words to dense distributional vectors that capture semantic meaning. Comparatively, the traditional methods represent words as atomic units, typically via a \textit{1 hot vector} \cite{frakes1992information}. These atomic representations do not capture semantic meaning. For example, the \textit{1 hot vectors} of ``dog'' and ``cat'' have no relation, but their respective word embeddings may capture that they are nouns, that they are both pets, and that they are similar. This can be vizualized using t-SNE\footnote{An excellent demonstration of t-SNE can be found at \cite{distill-tsne}.} \cite{maaten2008visualizing}. There are several techniques used to create word embeddings. The most popular options are Word2Vec \cite{mikolov2013distributed}, which is trained with contextual information of a sliding window of words across a corpus of documents, and GloVe \cite{pennington2014glove}, which is trained using global co-occurrence statistics among words. There has also been success with character-aware models \cite{kim2016character} that use convolutional neural networks to process the input that the character level. 

 Recent effective NLP models represent words with concatenated vectors from Glove \cite{pennington2014glove} and character-aware convolutional models \cite{kim2016character}. In \cite{seo2016bidirectional} these representations are concatenated together and then passed through a highway network \cite{srivastava2015highway}.

\begin{equation*}
    \tilde{x} = [ \text{Char-CNN}(word) ; \text{Glove}(word) ]
\end{equation*}
\begin{equation*}
    T(\tilde{x}) = \sigma(W_{(T)} \tilde{x} + b_{(T)})
\end{equation*}
\begin{equation*}
    x = T( \tilde{x}) *  \tilde{x} + (1 - T( \tilde{x})) * (W \tilde{x} + b)
\end{equation*}

This will encode semantic information derived from the co-occurrence statistics, as well as potential sub-word word information. This information may be from extracting morphemes or even capturing new out-of-vocabulary acronyms and names. Similarly, a recent work with ConceptNet \cite{speer2017conceptnet} encodes information beyond statistical measures. By using external sources, ConceptNet attempts to \textit{ground} word representations with pragmatic meaning. Specifically, rather than semantic meaning which is the meaning of a word given its context in language, (which corresponds with distributional and co-occurrence statistics), pragmatic meaning is directly tied to what the word means in the real world. Thus, ConceptNet attempts to encode knowledge graphs, characters, morphemes, and other embeddings like word2vec and GloVe. We do not use this in our work, but it is a potentially fruitful resource for future work.

\subsection{Attention}\label{sec:background-attention}

Effective models in difficult tasks like MultiSNLI and SQuAD use attention \cite{cao2017stacked,seo2016bidirectional, hu2017reinforced}. Attention directly represents the relevance of an entity to a task. Plausibly, it enables some separation of concerns; early modules in a larger network are free to encode the meaning and context and high-level attention layers determine an entity's relevance. There are different approaches to determining attention. In general, candidate entities' representations are scaled by a probability distribution. The probability distribution is derived from the similarity between the entities and some context vector or query. We will formalize our application of attention in \cref{sec:models}.

\include{notations}
