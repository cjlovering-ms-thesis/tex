\chapter{Machine Learning Models for Evidence Recommendation}\label{sec:models}

In this chapter we propose machine learning models for evidence recommendation.

\section{Problem Formulation}\label{sec:problem-formulation}

 After empirically evaluating different paradigms and aggregation methods, we found a comparison based method most effective. We extract a set of tasks $T = \{ (\tau_i) \}_{i=1}^N \mid \tau_i = \{ c_i, e^1_i, e^2_i \}$ from collected dataset (see \cref{sec:collection}) where $c_i$ is a claim, $e^1_i$ and $e^2_i$ are candidate evidence sentences that are to be directly compared. Each task $\tau_i$ is labeled by annotators as one of \texttt{0} or \texttt{1}. When $y_i$ is \texttt{0} it indicates $e^1_i$ is more relevant, and $e^2_i$ otherwise. Each task has multiple labels from different mechanical turk workers. Finally, we have a labeled dataset $D$ that consists of pairs of tasks $\tau_i$ and labels $y_i$. 
 
 \begin{equation*}
    D = \{ (\tau_i, y_i) \}_{i=1}^N  
 \end{equation*}
 
 We approach evidence recommendation by providing an order among a pair of candidate evidence sentences, i.e., denoting which sentence provides more evidence. Using this, we can recreate a global ranking among candidate evidence sentences to find the strongest pieces of evidence. One can either use a method to recreate a global ranking \cite{jagabathula2009inferring} or instead uses the outputted scores for each sentence to sort the sentences. We opt for the latter because of its simplicity.
 
\section{Loss function}\label{subsec:training}

 The objective for each model $\psi$ is to output a real value that corresponds to the relative rank of the item; $\psi: c \times e \times \theta \rightarrow \mathbb{R}$. The parameters of the model $\theta$ are learned through different training schemes and are dependent on the architecture of the model $\psi$. We elide $\theta$ from further notations for clarity. The loss for each model $\psi$ described below is as follows.

\begin{align}\label{eq:loss-dataset-pair}
 L&: D \rightarrow \mathbb{R} &L(D) = \frac{1}{|D|}\sum_{\tau_i, y_i \in D} L_i(\tau_i, y_i)
\end{align} 

In Eq. \ref{eq:loss-dataset-pair} we averaged the total loss across all labeled tasks.

\begin{align}\label{eq:lossi}
    L_{i}&: \tau \times y \rightarrow \mathbb{R}, & L_{i}(\tau, y) = \mathcal{H}(\psi(c, e^{0}), \psi(c, e^{1}))^{y} \times \mathcal{H}(\psi(c, e^{1}), \psi(c, e^{0}))^{(1-y)}
\end{align}

The loss for each annotated instance in the dataset is the hinge loss, Eq. \ref{eq:hinge}, of the model's score for the candidate evidence sentence that is labeled as providing more evidence compared to the evidence sentenced that is labeled as providing less evidence. 

\begin{align}\label{eq:hinge}
    \mathcal{H}&: s_1\times s_2 \rightarrow \mathbb{R}, &\mathcal{H}(s_1, s_2) = \texttt{max}(\texttt{0}, \xi + s_1 - s_2)
\end{align} 

Again, Eq. \ref{eq:hinge} is the hinge loss, with the margin $\xi$ \texttt{= 1}. However, in practice when we train our models we use a more compact definition (see Eq. \ref{eq:ranking-loss}). We keep the original definition for completeness.

\begin{align}\label{eq:ranking-loss}
    L_i^*&: \tau \times y \rightarrow \mathbb{R}, &L_i^*(\tau, y) &= \mathcal{H}^*(\psi(c, e^0), \psi(c, e^1), y)\\
    \mathcal{H}^*&: s_1\times s_2 \times y \rightarrow \mathbb{R}, &\mathcal{H}^*(s_1, s_2, y) &= \max(0, -y * (s_1 - s_2) + \xi)
 \end{align}

Eq. \ref{eq:ranking-loss} captures our requirements for the loss. A pair of compared scores $s_1$ and $s_2$ are compared. If $y$ is positive then the first score ($s_1$) should be ranked higher, otherwise $s_2$ should be ranked higher.

\section{Model Formulation for Direct Pairwise Comparison}\label{sec:pairwise-formulation}

\noindent This section covers the model definition for a model that processes a candidate evidence sentence and a claim sentence and is trained in a pairwise fashion. We use the pairwise loss defined above in Eq. \ref{eq:ranking-loss}.

\subsection{Base Implementation}\label{subsec:pairwise-model}

A recurrent network (a GRU or a LSTM) individually processes the claim and the candidate evidence sentence. The final hidden state for each sentence is concatenated together along with an element-wise multiplication of these vectors. This joined representation is then run through a linear layer. This is demonstrated in Figure \ref{fig:pairwise-concat}. This captures representations of both sentences and their interaction.

\begin{figure}[ht]
    \centering
    \includegraphics[width=\linewidth]{pairwise-concat}
    \caption{Sentence encodings are concatenated together.}\label{fig:pairwise-concat}
\end{figure}
   
In these figures \textsc{W-RNN} is one of RNN, GRU, or LSTM. For our initial versions we select the final hidden state as the sentence representation for both the claim sentence $c$ and the candidate evidence $e$.

\begin{figure}[ht]
    \centering
    \includegraphics[width=\linewidth]{rnn}
    \caption{Recurrent network overview.}\label{fig:rnn}
\end{figure}

It is common for recurrent networks to process sequences in both the forward and backward directions \cite{seo2016bidirectional}. Bi-directional models are often able to better capture semantic information. These output hidden states from both directions are concatenated together for the final hidden state output. The final hidden states for each direction when concatenated together have a dimensionality of $d_{(intra)}$. The word $intra$ refers to the intra sentence context that this recurrent network encoded.

Lastly, our approach to modeling the interaction between the claim and the evidence is one of several different operations we could have applied. Other options include concatenation, hadamard product, cross product, and cosine similarity between the final claim and evidence representations. We opted for the concatenation of the claim representation, evidence representation, and hadamard product of the claim and evidence representation.

Formally, our base implementation is defined below for a single task of a claim $c$ and evidence $e$. The $J$ words in the evidence $\{e_1, e_2, \dots e_J \}$ and the $I$ words in the claim $\{c_1, c_2, \dots c_I \}$ are the words are embedded using Glove \cite{pennington2014glove}. Each word vector is $\in R^{d_{(word)}}$ and the output dimensionality of the recurrent networks is $d_{(intra)}$. In our implementations, these are hyperparameters, but informally $d_{(word)} = 200$ and $d_{(intra)} = 80$. Note that a more powerful representation could be constructed by additionally using a character encoding as mentioned in \cref{sec:background}.

\begin{align}
    c &\in \mathbb{R}^{I \times d_{(word)}} \label{eq:claim_rep} \\
    e &\in \mathbb{R}^{J \times d_{(word)}} \label{eq:evidence_rep}
\end{align}
\begin{align*}
    C &= \text{BI-RNN}(c) \in \mathbb{R}^{I \times d_{(intra)}} \\ 
    E &= \text{BI-RNN}(e) \in \mathbb{R}^{J \times d_{(intra)}} \\
    C_{l} &= \text{SELECT-LAST}(C) \in \mathbb{R}^{d_{(intra)}} \\ 
    E_{l} &= \text{SELECT-LAST}(E) \in \mathbb{R}^{d_{(intra)}} \\ 
    V &= [ C_{l}; C_{l} \circ E_{l}; E_{l} ] \in \mathbb{R}^{3d_{(intra)}} \\ 
    y &= f_{(o)}(V) = W_{(o)}^\intercal V + b_{(o)} \in \mathbb{R}^1
\end{align*}

As we noted, the underlying RNN implementation can be either GRU or an LSTM. Both architectures have similar performances \cite{chung2014empirical} across many different tasks. We use an LSTM as defined in \cite{chung2014empirical}. For the bidirectional architectures, we use a SELECT-LAST function to extract and concatenate the last hidden state in terms of time step for both directions.

\subsection{Bidirectional Attention Flow for Ranking}

Below we present a formulation of attention modified from Bidirectional Attention Flow (BiDaf) \cite{seo2016bidirectional}. In this section we describe how we create a contextual representation of the task, that accounts for what is important in the evidence with respect to the claim and what is important to the claim with respect to the evidence. This formulation is more natural for our problem than learning a general contextual vector that is used to apply attention (as in \cite{yang2016hierarchical}). A generalized context vector is less applicable in the case where scores for task are relative only to the input (the claim), not predetermined classes (like a static representation of a cat). This model architecture is illustrated in Figure \ref{fig:attention-rnn-2}.

\subsubsection*{Word Representation}

The claim $c$ and the evidence $e$ are represented as sequences of word embeddings (like in Eqs. \ref{eq:claim_rep}, \ref{eq:evidence_rep}).

\subsubsection*{Contextual Embedding}

The contextual information of both the claim and the evidence is captured via a bi-directional recurrent network. Again, this is the same formulation as the base model. Note that this layer of the model is bi-directional the output dimensionality for each time step would be $2d_{(intra)}$, but the hidden size for each direction is set to $\frac{1}{2}d$. Claims and evidence are processed by different recurrent layers (i.e. there is no weight sharing). However, we do not use the SELECT-LAST function, and instead will operate on the entire vector of hidden states.

\subsubsection*{Attention}

A similarity matrix $S \in \mathbb{R}^{I \times J}$ is constructed, where $S_{ij}$ is the similarity between the representation of claim word $i$ and evidence word $j$. For clarity, $C_i$ and $E_j$ refer to the $i^{th}$ and $j^{th}$ contextually encoded word embeddings for claim word $i$ and evidence word $j$.

\begin{equation*}
    S_{ij} = f_{(s)} (C_{i}, E_{j}) \in \mathbb{R}^1.
\end{equation*}

This similarity is calculated by a learned affine function $f_{(s)}$.

\begin{align*}
    f_{(s)}&: \mathbb{R}^{d_{(intra)}} \times \mathbb{R}^{d_{(intra)}} \rightarrow \mathbb{R}, 
    &f_{(s)}(u,v) &= w_{(s)}^\intercal [ u ; v ; u \circ v ] + b_{(s)}.
\end{align*}

Next, we construct attended representations with attention flowing from evidence to claim and claim to evidence. This was an important idea from \cite{seo2016bidirectional}. However, our representation is different because our task has a simpler output (a score), and thus only requires a summarization of the context.

\begin{align*}
    \alpha_{(c)} &= \text{softmax} (\max_{row}(S)) \in \mathbb{R}^J \\
    \grave{e} &= \sum_j \alpha_{(c)j} C_{j} \in \mathbb{R}^{d_{(intra)}}
\end{align*}

The same is computed for the claim.

\begin{align*}
    \alpha_{(e)} &= \text{softmax} (\max_{col}(S)) \in \mathbb{R}^I \\
    \grave{c} &= \sum_i \alpha_{(e)i} E_{i} \in \mathbb{R}^{d_{(intra)}}
\end{align*}

\subsection*{Modeling}

We model the attended vectors with concatenation. There are a lot of other options here, but simple concatenation was effective in \cite{seo2016bidirectional}. The authors of \cite{seo2016bidirectional} also included the previous unattended representations, but we opt for a more simplified approach. There is room in future work to explore this decision further.

\begin{equation}\label{eq:final-sen-rep}
    v = [ \grave{e} ; \grave{c} ; \grave{e} \circ \grave{c} ] \in \mathbb{R}^{3 * d_{(intra)}}
\end{equation}

\subsubsection*{Output}

The final output layer $f_{(o)}$ introduces a wealth of possible formulations. We use a simple affine function.

\begin{align*}
    & f_{(o)}: \mathbb{R}^{3d_{(intra)}} \rightarrow \mathbb{R}^1, & f_{(o)}(v) =  w_{(o)}^\intercal v + b_{(o)}
\end{align*}

Another reasonable option would be a dot product with a learnable weight vector without a bias. If we find that the output should have a restricted domain a reasonable choice would be $\tanh(.)$. 

\begin{figure*}[ht]
    \centering
    \includegraphics[width=\linewidth]{attention-rnn-2}
    \caption[Bidirectional Attention Flow]{Attention that flows from both evidence to claim and claim to evidence. Best viewed in color.}\label{fig:attention-rnn-2}
\end{figure*}

\section{Document-based Model Formulation}\label{sec:document-formulation}

 This section covers the goal, loss, and model definition for a model that processes an entire document and a claim. The goal of our model is to determine a total ordering among all sentences in a document to a given claim. Any discrepancy between labels of sentences is an error, and not indicative of a partial ordering. We model this in our model by outputting a single score in scalar space for each sentence. 

 Specifically, we are given a document D consisting of L evidence sentences $e$, each of varying number of words $T_l$ from $l = 1 \dots |L|$. A subset of sentences in this document are compared in a binary fashion, where the sentence that contained more evidence was labeled as such. This is not a complete set of combinations.

\begin{equation*}
    D = \{ e_l \}_{l=1}^L \mid T_l = |e_l|
\end{equation*}

We have a set of annotations $A^{c}_D$ (eq. \ref{eq:A}) with respect to claim $c$ for a given document $D$. Each annotation consists of a task $\tau$ and a label $y$.

\begin{equation}\label{eq:A}
    A^{c}_D = \{ (\tau_i, y_i) \}_{i=1}^N \mid \tau_i = \{ e^1_i, e^2_i \}
\end{equation}

 For a task $\tau$, $e^1$ and $e^2$ are candidate evidence sentences that are to be directly compared. Each task $\tau_i$ is labeled by annotators as one of \texttt{0} or \texttt{1} by $y_i$. When $y_i$ is \texttt{0} it indicates $e^1_i$ is more relevant, and $e^2_i$ otherwise. This is the same as in the pairwise formulation \cref{sec:problem-formulation}

 We develop a model which encodes sentences and then ranks the relative importance of all sentences. The encoding is done hierarchically, first at the word level and then the sentence level. This approach is similar to \cite{yang2016hierarchical}, however we have an ordinal ranking head which outputs a value for each sentence. We detail the model in \cref{subsec:model-details}.

 The best training regime for this model is not immediately clear. There are two primary complications. First, the training labels within a document are sparse. Second, even for the labels we do have, they are binary comparisons and as such the loss cannot be immediately evaluated when the score is regressed. As described in below in \cref{sec:document-training-loss}, we process the entire document and incur a loss only for annotated sentences in the document.

\subsection{Loss function}\label{sec:document-training-loss}

 The objective for our model $\psi$ is to output a real value that corresponds to the relative rank of the item for each of the $L$ candidate evidence sentences in a document.
 
 \begin{equation*}
    \psi: D \times c \times \theta \rightarrow \mathbb{R}^L
 \end{equation*}
 
 The parameters of the model $\theta$ are learned and are dependent on the architecture of the model $\psi$. We elide $\theta$ from further notations for simplicity. The loss for each model $\psi$ described below is as follows for an annotated corpus $C = \{  A^{c}_{Di}, D_i \}_{i=1}^N$ of $N$ sets of annotations $A$ with respect to a claim $c$ and document $D$.

 \begin{align}
    &L: C \rightarrow \mathbb{R}, & L(C) &= \frac{1}{N}\sum_{A^{c}_{D}, D \in C} L_D(A^{c}_{D}, \psi(D,c)) \label{eq:lossc} \\
    &L_D: A^{c} \times \hat{Y} \rightarrow \mathbb{R}, & L_D(A^{c}, \hat{Y}) &= \frac{1}{|\tau, y \in A^c_D|}\sum_{\tau, y \in A^c_D} L_\tau(\tau, y, \hat{Y}) \label{eq:loss-dataset-document} \\
    L_\tau &: \tau \times y \times \hat{Y} \rightarrow \mathbb{R}, &L_\tau(\tau, y, \hat{Y}) &= \mathcal{H}^*( \hat{Y}_{e^1} , \hat{Y}_{e^2}, y) \label{eq:document-task-loss}
\end{align} 

The loss is averaged across the corpus as shown in Eq. \ref{eq:lossc}. For each set of annotations, the model $\phi$ computes the scores for all the sentences in the document. Then the loss is averaged across all the tasks in the set of annotations in Eq. \ref{eq:loss-dataset-document}. From here, $L_\tau$ is the very similar to the loss used pairwise model (see Eq. (\ref{eq:lossi}). Specifically, as shown in Eq \ref{eq:document-task-loss}, it instead indexes the results from the vector of scores for each sentence $\hat{Y}$ computed by $\phi$. The hinge loss $\mathcal{H}^*$ is defined above in Eq. \ref{eq:lossi}.

\subsection{Hierarchical Attention for Document-Wise Rankings}\label{subsec:model-details}

The document models are built on top of our pairwise approach, and evaluated in a pairwise manner. Note, that this model builds on top of the pairwise formulation defined in \cref{sec:pairwise-formulation}. This formulation is similar to that describe by \cite{yang2016hierarchical}. 

\subsubsection*{Sentence Representation}

The sentences are represented as in \cref{sec:pairwise-formulation}. Note that the evidence dimension now includes the number of sentences in the cited document $L$. In the following equations, $I$ is the number of words in the claim sentence and $J$ is the maximum number of words in a candidate evidence sentence.

\begin{align*}
    c &\in \mathbb{R}^{I \times d_{(word)}} \\
    e &\in \mathbb{R}^{L \times J \times d_{(word)} } \\
\end{align*}

After encoding the words, we encode the intra sentence context using bidirectional recurrent networks. The difference between this and the approach in \cref{sec:pairwise-formulation} is that the claim is first repeated (tiled) for each of the sentences in the document.

\begin{align*}
    C &= \text{BI-RNN}(c) \in \mathbb{R}^{I \times d_{(intra)} } \\ 
    C_{tiled} &= [ C | x \in 1 \dots L ] \in \mathbb{R}^{L \times I \times d_{(intra)}} \\
    E &= \text{BI-RNN}(e) \in \mathbb{R}^{L \times J \times d_{(intra)}} \\
\end{align*}

As above, we apply \textbf{intra-sentence attention} with the bidirectional attention between the words within each candidate evidence sentence and the claim. The resulting representations are summarized across the number of word dimensions.

\begin{align*}
    \grave{e} &= \text{BIDIRECTIONAL-ATTENTION}(E, C_{tiled}) \in \mathbb{R}^{L \times d_{(intra)}} \\
    \grave{c} &= \text{BIDIRECTIONAL-ATTENTION}(C_{tiled}, E) \in \mathbb{R}^{L \times d_{(intra)}}
\end{align*}

We then model the sentences representations in the document with our standard approach of concatenating the tensors and their element-wise product.

\begin{align*}
v &= [ \grave{e}; \grave{e} \circ \grave{c}; \grave{c} ] \in \mathbb{R}^{L \times 3d_{(intra)}} \\
\end{align*}

\subsubsection*{Inter-Sentence Contextual Embedding}

We follow the same pattern as in the pairwise formulation; the context between the sentences is encoded using a recurrent network. This works in the same way as the contextual embedding between words. The output dimensionality of the inter sentence contextual embedding for each sentence is $d_{inter}$.

\begin{align*}
    V &= \text{BI-RNN}(v) \in \mathbb{R}^{L \times d_{(inter)}} \\
\end{align*}

\subsubsection*{Inter-Sentence Attention}

The attention at the sentence level is based off a learned representation. We use a formulation of attention, as in \cite{yang2016hierarchical}, where $c$ is a learned vector. We do not reuse a representation of the claim again here because the network has already encoded the claim into its representation. This learned vector enables the attention mechanism to determine if a given sentence successfully provides evidence for the claim it incorporates.

\begin{align*}
    U_{(a)} &= \tanh (W_{(a)}^\intercal V + b_{(a)}) \in \mathbb{R}^{L \times d_{(inter)}} \\
    \alpha_{(a)} &= \text{softmax}(U_{(a)}^\intercal c) \in \mathbb{R}^{L \times d_{(inter)}} \\
    \grave{v} &= V \circ \alpha_{(a)} \in \mathbb{R}^{L \times d_{(inter)}}
\end{align*}

\subsubsection*{Modeling}

The final layer is a learnable affine layer.

\begin{align*}
    y &= f_{(o)}(\grave{v}) = W_{(o)}^\intercal \grave{v} + b_{(o)} \in \mathbb{R}^L \\
\end{align*}

\subsection*{Model Definition}

The complete model architecture, with high-level operations for the different modules, is shown below.

\begin{align*}
    c &\in \mathbb{R}^{I \times d_{(word)}} \\
    e &\in \mathbb{R}^{L \times J \times d_{(word)} } \\
    C &= \text{INTRA-SENTENCE-EMBEDDING}(c) \in \mathbb{R}^{I \times d_{(intra)} } \\ 
    C_{tiled} &= [ C | x \in 1 \dots L ] \in \mathbb{R}^{L \times I \times d_{(intra)}} \\
    E &= \text{INTRA-SENTENCE-EMBEDDING}(e) \in \mathbb{R}^{L \times J \times d_{(intra)}} \\
    \grave{e} &= \text{INTRA-SENTENCE-ATTENTION}(E, C_{tiled}) \in \mathbb{R}^{L \times d_{(intra)}} \\
    \grave{c} &= \text{INTRA-SENTENCE-ATTENTION}(C_{tiled}, E) \in \mathbb{R}^{L \times d_{(intra)}} \\
    v &= [ \grave{e}; \grave{e} \circ \grave{c}; \grave{c} ] \in \mathbb{R}^{L \times 3d_{(intra)}} \\
    V &= \text{INTER-SENTENCE-EMBEDDING}(v) \in \mathbb{R}^{L \times d_{(inter)}} \\
    \grave{v} &= \text{INTER-SENTENCE-ATTENTION}(V) \in \mathbb{R}^{L \times d_{(inter)}} \\
    y &= f_{(o)}(\grave{v}) = W_{(o)}^\intercal \grave{v} + b_{(o)} \in \mathbb{R}^L \\
\end{align*}
