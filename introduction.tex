\chapter{Introduction}\label{sec:introduction}

The focus of this work is creating a model that recommends the sentences in a referenced document that provide the most evidence for a given claim. We name the process of recommending evidence for a claim as \textit{evidence recommendation}. By focusing human attention to the most relevant portion of the cited text, an application of our model can save human effort. This work will help researchers with literature reviews and reviewers check conference submissions by automatically finding corroborating evidence to a claims in a referenced documents. Evidence recommendation can also help a reader better understand previous work that supports the citing document by providing evidence the reader may have missed or would not have had time to read. Evidence recommendation could also help readers determine which paper best supports a claim and if a paper was cited erroneously.

We analyze text at the sentence level. We define a \textit{claim} to be a sentence citing material from another paper. Likewise, \textit{evidence} for a given claim is a sentence in a referenced paper that corroborates the assertion made by the claim. Each citing document can have multiple claims. Each claim can refer to multiple different referenced documents, and each cited document can have multiple pieces of evidence.

\section{Motivating Example}

When Jane Doe is reviewing a paper on the trustworthiness of classifiers \cite{ribeiro2016should}, she reads the following claim:

\begin{quote}
  \texttt{Data leakage, for example, defined as the unintentional leakage of signal into the training (and validation) data that would not appear when deployed  \cite{kaufman2012leakage}, potentially increases accuracy.}
\end{quote}

If she is unfamiliar with ``data leakage'', she may turn to the referenced paper \cite{kaufman2012leakage}. Reading the entirety of the referenced paper would help her to confirm the definition and the motivation behind the term, but would require a lot of time. If she instead could look at a thresholded heat map on the paper where the color indicates a particular sentence likely has evidence that supports the claim, as demonstrated in Fig (\ref{fig:example_2}), she would be able to directly locate evidence to the claim. This will help Jane quickly find the corroborating evidence, and better understand the underlying material.

 \begin{figure}[ht]
  \centering
  \includegraphics[width=0.90\linewidth]{motivating.pdf}
  \caption[Demonstration of thresholded heat map.]{Demonstration of a thresholded heat map on the entirety of an academic paper where the temperature highlights potential \textit{evidence} for a given \textit{claim}. Left, is a zoomed-in example of candidate sentences. Right, is an overview heatmap that would allow a reader to locate potential evidence.}\label{fig:example_2}
 \end{figure}

\section{Related Work}\label{sec:related-work}

An automated tool for evidence recommendation for academic papers has never been made to the best of our knowledge. There are many similar citation recommendation systems \cite{he2010context, kuccuktuncc2015diversifying, ritchie2009citation} that recommend papers to cite for some text as a utility for researchers. Some of these recommendation systems utilize the text surrounding the citation \cite{alzahrani2012using, he2010context}. This surrounding text is called the context \cite{alzahrani2012using, he2010context}, and corresponds to term \textit{claim} in this proposal. Alzahrani et al. \cite{alzahrani2012using} used the context for plagiarism detection and Aya et al. \cite{aya2005citation} used the context to classify the citation itself as one of evidence, motivation, etc. These works operate on document level statistics, and as we work directly with sentences, we look at other works for guidance.

Question Answering \cite{rajpurkar2016squad} and Natural Language Inference \cite{bowman2015large} are two open problems in Natural Language Processing that are directly related to our task. Question answering (QA), a machine learning task where a model tries to find the answer to a question from within multiple documents, is similar to evidence recommendation. In fact, evidence recommendation is an instance of QA. The question takes the form of: ``What sentences provides evidence for this \textit{$\{claim\}$}?''. There are many different research efforts focused on improving QA models. The recent SQuAD dataset \cite{rajpurkar2016squad} has resulted in the creation of numerous new models \cite{rajpurkar2016squad, wang2017gated, liu2017stochastic, hu2017reinforced}.

It has been argued that models capable of Natural Language Inference (NLI) \cite{bowman2015large} capture important semantic information and demonstrate some level of reasonable language understanding. NLI is the task of determining the relationship between sentences; whether a sentence contradicts, entails or is has neutral relationship to the another. Recent efforts in models for NLI \cite{tay2017compare, shen2018reinforced, chen2017recurrent, nie2017shortcut} are being driven by the release of the SNLI \cite{bowman2015large} and MultiSNLI \cite{nangia2017repeval} datasets. For us to be successful in this project, our model must be capable of NLI because evidence recommendation requires quantifying the extent a sentence entails another sentence. This is complicated by the fact that surrounding sentences may provide either a contradictory or supportive contextual information. For example, an example \textit{text} and entailed \textit{hypothesis} from the SNLI dataset are reproduced below.

\begin{table}[htbp]
  \caption{Example of sentences from SNLI Dataset}
  \renewcommand{\arraystretch}{1.5}
  \begin{tabular}{l|l}
  \textbf{Premise}  & \textbf{Hypothesis} \\ \hline
  \begin{tabular}[t]{p{7cm}} A soccer game with multiple males playing. \end{tabular} & \begin{tabular}[t]{p{7cm}} Some men are playing a sport.\end{tabular}
  \end{tabular}
\end{table}

\noindent For the SNLI task a model must infer that soccer is a sport and that multiple males are men. In evidence recommendation, the descriptions earlier in a document may have to be interpreted for the evidence provided by the sentence to be fully evaluated; i.e. the context of the document as a whole is important. A hypothetical example of this is shown below.

\begin{table}[htbp]
  \caption{Hypothetical Example of Evidence Recommendation}
  \renewcommand{\arraystretch}{1.5}
  \begin{tabular}{l|l}
  \textbf{Evidence (Premise)}  & \textbf{Claim (Hypothesis)} \\ \hline
  \begin{tabular}[t]{p{7cm}}   The children went outside. The males started playing a soccer game. That night their fathers played tennis.
  \end{tabular} & \begin{tabular}[t]{p{7cm}} Some men are playing a sport.\end{tabular}
  \end{tabular}
\end{table}

\noindent In this example, the second sentence (modeled after the text from SNLI) is not evidence for the claim because the males refers to children not adults. The last sentence is evidence because it does entail that male adults are playing a sport. This example indicates some of the difficulties of ER, like the importance of context, but the recent success in both QA and NLI inform us how to approach modeling this type of problem.

There has been some recent work \cite{hua2017understanding} in argument analysis that parallels our work. The application and focus of Hua et al. is to classify different arguments used in a debate. Fundamentally, this also involves finding and understanding evidence. There are some subtle differences in that argument classification focuses primarily on the type of argument, not necessarily its strength or relative importance. We will leverage their dataset to compare and evaluate our models in a different context.

Lastly, we utilize pairwise annotations as detailed in \cref{sec:annotation}. In the learning to rank paradigm, the task of finding order among items, training models on pairwise annotations is a common approach \cite{burges2010ranknet}. Previous work in learning to rank is represented by Burges et al. with LambdaRank and LambdaMART \cite{burges2010ranknet, burges2007learning}. In this work, we attempt to wed the semantic interpretation of our NLI-based model, with the capacity of a ranking algorithm. Further exploration in fully utilizing these ranking methods, as mentioned in \cref{sec:future_work}, is an exciting area for future work.

\section{Challenges}

There are multiple approaches to question answering, and relevant underlying methods like classification and ranking, but exactly how to formulate evidence recommendation is unclear because the application is novel. This consists of two primary challenges.

\begin{itemize}
  \item Formulating a labeling task that ordinary people working on crowdsourcing websites can perform to provide us with meaningful labels.
  \item Developing a machine learning model that is able to automatically recommend evidence.
\end{itemize}

\section{Scope of Work}

We are interested in the semantic correlation between sentences. So, our approaches do not focus on matching numeric values. However, this is an important feature that could be added to a final product. Also, we are not concerned with determining the actual reason a publication was cited (e.g. for comparison, for evidence, etc.) or why a particular publication was chosen instead of other similar previous works. In addition to the citation recommendation described above, scientometrics \cite{leydesdorff2012scientometrics} studies the impact of publications and the automatic indexing of citations \cite{giles1998citeseer}. These concerns are related to our work, but are a different type of problem that we do not try to solve.

\section{Approach}\label{subsec:approach}

We determine how to formulate our problem empirically. Candidate options include framing the task as classification, where labels indicate whether or not a sentence is a piece of evidence, and ranking, where the best evidence is labeled as most important. Classification allows for a more precise approach as evidence would be directly labeled as such. Ranking alternatively offers an ordinality among pieces of evidence that may better allow users to more quickly find the best evidence. We annotate our dataset via pairwise comparisons, as detailed in \cref{sec:annotation}, because of the higher labeling accuracy that we obtain with ranking compared to classification. We propose exploring a range of deep neural models and building a prototype application that utilizes our trained model. Our approach can be broken down into the following tasks:

\begin{enumerate}
  \item Define a data collection methodology for collecting a set of academic papers and their referenced academic papers.
  \item Develop annotation tasks and select an annotation aggregation technique.
  \item Determine our machine learning task based on our evaluation of different annotation techniques.
  \item Design models for evidence recommendation.
  \item Deploy a prototype application utilizing constructed models.
  \item Evaluate our results.
\end{enumerate}

\section{Remainder of the Document}

Background to relevant machine learning is given in \cref{sec:background}. The data collection methodology is detailed in \cref{sec:collection}. We evaluate the different ways of annotating our dataset in \cref{sec:annotation}. We empirically find that ranking is an effective formulation in \cref{subsec:quality} for our problem. We define our problem formulation and propose model architectures in \cref{sec:models}. The evaluation for our models and our proposed prototype application (see \cref{sec:prototype}) is in \cref{sec:eval}. The evaluation is presented in \cref{sec:results}. We describe future work in \cref{sec:future_work} and conclude in \cref{sec:conclusion}. Lastly, the notation used in this document is defined in appendix \ref{sec:notation}.
