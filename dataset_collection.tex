\chapter{Dataset Collection}\label{sec:collection}

We collect a corpus of academic papers. We will need to annotate the dataset we collect, and this is described in \cref{sec:annotation}. Annotation requires a massive amount of human labor. We explore crowdsourcing the annotation tasks to workers with Amazon Mechanical Turk \cite{amazon} because of its potential to scale at a low cost. For the academic discipline with explore evidence recommendation with, we chose education. We do not expect our workers to have deep insights into the subject matter. However, we do anticipate that the terminology used in educational research papers will be more accessible than the jargon from other domains like theoretical physics or mathematics. Even though our workers may not fully grasp a paper's meaning, we hypothesize that they will understand enough to provide annotations that a machine learning classifier can use to automatically find relevant evidence.

We collected 500 papers from American Educational Research Association (AERA) \cite{aera}. The academic papers are converted into raw text from PDF using pdfminer \cite{pdfminer}. From here, we tokenized the documents into sentences using NLTK \cite{bird2009natural} (a python library for text processing). We then parsed references and citations from the documents using regular expressions. We compared the results of our regular expressions against results from ParsCite \cite{pars}, and found our more direct approach more effective in finding valid citations for our dataset which uniformly uses the APA format. In a pilot analysis of a small sample of files, we found our direct method was able to find and correctly parse a higher percentage of the citations. As the focus of this proposed study is the capability to recommend the best sentences, and not (at least initially) a generalized end-to-end system that parses and fetches documents, we move forward with our simplified approach.

After parsing these references from our initial 500 papers, we downloaded a subset of all the referenced documents. Full details can be seen in Table \ref{table:dataset-document-statistics}. The average number of sentences per referenced document is 210. 

\begin{table}[t]
 \caption{Summary of Document Statistics}
 \label{table:dataset-document-statistics}
 \centering
 \begin{tabular}{ll}
   \toprule
   \cmidrule{1-2}
   {Description}     & {Size} \\
   \midrule
   Number of referencing documents & 500 \\
   Number of referenced documents  & 1,100 \\
   Average number of sentences per referenced document & 210 \\
   \bottomrule
 \end{tabular}
\end{table}

In order to reduce the number of sentences we need to label, we sampled sentences by their similarity from the referenced document to the claim. We do this because otherwise the cost to label all the sentences would be prohibitively high.

We determined similarity by using InferSent \cite{conneau2017supervised}, a model which utilizes GloVe \cite{pennington2014glove} word embeddings, and embeds sentences into vectors that capture semantic meaning. We selected evidence sentences to label based upon their cosine similarity to the corresponding claim's vector via $f_{sim}: c \times e \rightarrow \mathbb{R}$  as shown in Eq. \ref{eq:sim}.

\begin{align}
  &c = \texttt{InferSent}(claim) \nonumber \\
  &e = \texttt{InferSent}(evidence) \nonumber \\
  &f_{sim}(c, e) = \cos(c, e) \label{eq:sim}
\end{align}

In a pilot analysis we investigated the efficacy of using InferSent to capture the best pieces of evidence in a document within best \textit{k} sentences. To do this, we hand-labeled a small sample of 5 different documents. We labeled the sentences to find the pieces of evidence in the document, so each sentence was labeled as evidence or not. We then found that when we ordered the sentences by similarity using InferSent, the top 10 sentences capture most of the evidence. This is shown in Figure \ref{fig:infersent}. This is not a rigorous experiment, as this manual labeling is prohibitively expensive, but it gives us confidence in our sampling methodology.

\begin{figure}[!bph]
   \centering
   \includegraphics[width=0.60\textwidth]{infersent-pilot.pdf}
   \caption[Pilot Study on Basic Metric]{Red symbols indicate the sentences that provide the most important evidence to a claim within a document. Symbols are differentiated for clarity. Each line represents all the sentences in a distinct document. }
   \label{fig:infersent}
\end{figure}

\section{Dataset Collection Discussion} 
 
 After collecting this dataset we have a large amount of data that pertains to our task, i.e. the content of papers that cite each other. We also have selected a subset of the relevant sentences for labelling. However, this is not sufficient for training machine learning models for evidence recommendation. In the next section (\cref{sec:annotation}) we discuss how we label this dataset.

