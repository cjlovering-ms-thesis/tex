\chapter{Dataset Annotation}\label{sec:annotation}

 The technical challenge we address in this section is how to define our problem. Our goal is to create a model that can determine which sentences in a referenced document support a claim from the corresponding referencing document. However, the machine learning task utilized to do this is flexible. The best machine learning task for evidence recommendation is an empirical question that hinges on how effective human labelers are on that task for our data. For us this is particularly important because the task is difficult, and so it is advantageous for us to frame the task in the way that works best for the labelers.

 We explore two approaches for our task. First is classification. Classification is the giving an entity a discrete label from a predetermined set. Ranking is ordering items into some desired ordering. Ranking methods often are simplified to comparing pairs of items to determine which is more relevant. From here, the comparisons can be used to create a global ranking directly \cite{jagabathula2009inferring}. Alternatively, scores given to each sentence can be used to sort the sentences into a ranking. For each of these methods we need to annotate the dataset in different ways. The specifics for each method vary, but we can compare the quality of different annotation configurations using metrics. 

 We also compare two aggregation methods. For each label, we have multiple annotators label that task. The methods we compare is majority vote and GLAD \cite{whitehill2009whose}. Majority vote takes the mode class between an odd number of votes, whereas GLAD determines the most likely label by harnessing the inter-labeler agreement among labelers to simultaneously infer the expertise of each labeler, and the difficulty and ground-truth of each item. These methods help smooth the noise from the annotations of malicious, unskilled, or simply incorrect workers.

\section{Preliminary Unsatisfactory Results}

Before beginning our formal experimentation we quickly found negative results with other methods. We include them here for completeness.

\paragraph{Unsatisfactory results with 4-class classification}

We also tried classification with 4 classes and found those labels to be random with Cohen scores of near 0 and area under AUC near 0.5.

\paragraph{Unsatisfactory results with entire-document tasks} 
We first set the task as finding the single best piece of evidence in a referenced document. However, our experiments indicated that this task did not achieve a high throughput in terms of worker interest, and that answers we received were always just the first approximately related sentence in the document, if not random. 

\section{Experimental Design of Comparable Paradigms}\label{subsec:experiment}

 To compare these methods, we build user interfaces for annotators to label sentences. For each configuration we manually label a small validation dataset. For each task, we have annotators label it multiple times. We will compare aggregations and paradigms by evaluating the results across several holistic metrics (described below).
 
 \paragraph{Metrics} We compare the annotation configures across the cohen score, the ROC, and the F1 Score. The cohen score is a measure of annotator agreement \cite{cohen1960coefficient} which has a range from -1 to 1 where less than 0 indicates random annotations and 1 is perfect agreement. The area under the AUC is a metric to evaluate classifier output quality and the F1 scores is the harmonic mean between precision and recall. AUC is invariant to class imbalances \cite{frakes1992information}, and F1 scores are comprehensive in that its value captures more nuance than accuracy.

\section{Task Definition}\label{subsec:ui}

\paragraph{Binary Labels for Classification}

We compare binary classification (BC) of a sentence into two different sets of discrete labels,  \texttt{\{ relevant, not relevant \}}(BC-R) and \texttt{\{ evidence, not evidence \}} (BC-E). Each of these result in valuable annotations, but it is unclear which *wording* more clear to a labeler. Figures \ref{fig:evd} and \ref{fig:rel} show both annotation tasks for classification. These two tasks are similar with variations in the instructions and the prompt.

\paragraph{Binary Comparison for Ranking Labels}

Given two candidate sentences, and the evidence sentence, the task is to determine which sentence provides more evidence. The interface for the binary ranking (BR) task is similar to the classification interfaces, but instead presents two candidate evidence sentences as shown in Figure \ref{fig:ranking}. 

\section{Evaluation of Annotation Tasks}\label{subsec:quality}

We compare two paradigms, binary ranking (BR) and binary classification (BC), in Table \ref{table:prelim}. We get these results from evaluating annotations from crowdsourced tasks versus our ground truth annotations. Binary ranking outperformed binary classification across all three metrics. The best result, ranking with labels aggregated by majority vote on 5 votes, has a AUC is 0.69.

\begin{table}[!ht]
  \caption{Annotation Task Evaluation}
  \label{table:prelim}
  \centering
  \begin{tabular}{clccc}
    \toprule
    {Paradigm} & {Aggregation} & {Cohen Score} & {AUC} & {F1 Score} \\
    \midrule
    BR & Vote 3 & 0.29 & 0.64 & 0.64 \\
    BR & Vote 5 & \textbf{0.39} & \textbf{0.69} & \textbf{0.69} \\
    BR & GLAD 3 & 0.22 & 0.61 & 0.61 \\
    BR & GLAD 4 & 0.19 & 0.59 & 0.59 \\
    BR & GLAD 5 & 0.26 & 0.63 & 0.63 \\
    BC-E & Vote 3 & 0.16 & 0.61 & 0.55 \\
    BC-R & Vote 3 & 0.04 & 0.52 & 0.56 \\
    \bottomrule
  \end{tabular}
   \caption[Dataset Annotation Task Results]{These results are a comparison of annotations from crowdsourced tasks compared to our ground truth annotations. BR is binary ranking used for the ranking paradigm. BC-* is binary classification. BC-E task asks whether a sentence is evidence or not. BC-R task asks whether a sentence is relevant or not. Both tasks used the same data, but required different number of questions and tasks. There are 100 comparisons for BR and 115 tasks for BC. The aggregation indicates how the labels were formed: Vote indicates majority vote, whereas GLAD uses the optimal aggregation method \cite{whitehill2009whose}. The number that follows indicates the number of annotations per task that were aggregated. }
\end{table}

\section{Dataset Details}\label{sec:dataset-details}

We used pairwise annotations to label our dataset. So far, we have had 7265 tasks labeled. We have aggregated this into 1453 majority-vote labels. Below is a summarization of our dataset and notes on how we ensured no data leakage between splits.

\begin{enumerate}
\item We balanced the dataset so there are exactly equal number of positive and negative items, by reordering the items in a binary comparison. This allows us to look at accuracy as a meaningful metric without skew due to unbalanced class distributions.
\item We stratified the dataset so there is no crossover of referenced or referencing documents between train, validation and test splits. To do this, and split the partitions with appropriate sizes, we use a standard knapsack packing algorithm.
\item We created an \textit{augmented} dataset with relatively cheap data points. For items that were only ranked positively, we added binary comparisons where these positively ranked items are positively compared versus a sample of points that were previously discarded from within the document as less relevant. This allows us to automatically grow our dataset. However, this reduces the meaning of the labels.
\item We examine our models performance on other aggregation techniques as well. Namely, 
we reduced our primary dataset to a smaller subset termed ``high majority'' where we only kept items that were voted at a rate of at least 4 out of 5. We also examine ``unanimous'' where we only keep items that have labels agreed upon by all workers.
\end{enumerate}

The current magnitudes of the dataset are presented in Table \ref{table:dataset-item-counts}. We could increase the size of the augmented dataset almost arbitrarily.

\begin{table}[tp]%
    \caption{Dataset Item Counts}
    \label{table:dataset-item-counts}\centering%
    \begin{tabular}{lccc}
        \toprule[1pt]
        Dataset & Train Size & Validation Size & Test Size \\
        \midrule[0.5pt]
        majority & 1017 & 201 & 256 \\
        high majority & 457 & 111 & 93 \\
        unanimous & 109 & 26 & 20 \\
        augmented & 7780 & 1565 & 1393 \\
        \bottomrule[1pt]
    \end{tabular}
\end{table}

We show the counts for the document versions annotations below in Table \ref{table:dataset-document-counts}.

\begin{table}[tp]%
    \caption{Document Dataset Sizes}
    \label{table:dataset-document-counts}\centering%
    \begin{tabular}{lccc}
        \toprule[1pt]
        Train (\# of Documents) & Validation (\# of Documents) & Test (\# of Documents) \\
        \midrule[0.5pt]
        citation & 52 & 11 & 14\\
        \bottomrule[1pt]
    \end{tabular}
\end{table}