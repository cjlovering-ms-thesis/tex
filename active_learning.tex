\chapter{Active Learning}\label{sec:active}

Increasing our dataset with high quality labels in a complete manner requires an intractable amount of financial resources. However, if we employ active learning we may be able to supplement our current set of labels with a selected set of labels that will make the best improvement to our model. There are a few different schemes we could try.

Active learning is the idea of allowing the machine learning model determine which data should be labeled, in order to get better performance from fewer labels. There are a few different approaches: uncertainty reduction, error reduction, and ensemble-based \cite{RubensRecSysHB2010} summarized in Table \ref{table:active}.

At a high level, we could select additional data from unseen claim and referenced document pairings, or we could select unlabeled pairings among already annotated pairings. The intuitive tradeoff here is that gathering more annotations from unseen documents may increase the generalization of the network, whereas the later option may provide greater precision. Lastly we could apply whatever manner evaluation (see next) to both data sources.

Next, we must determine how to choose which comparisons to annotate. Here we detail the different approaches.

\begin{table*}[tp]%
  \small
    \caption{Approaches to Active Learning}
    \label{table:active}\centering%
    \hspace*{-1cm}\begin{tabular}{cp{5cm}p{5cm}}
      \toprule[1pt]
      {Approach} & {Description} & {Consideration} \\
      \midrule[0.5pt]
    Uncertainty Reduction 
    & Reduce uncertainty of estimates and decision boundaries.
    & Does not consider the fact that the most nefarious data points are those where model is both confident and incorrect.
    \\
    Model Impact
    & Choose the points that would cause the most change in the model.
    & \textit{Explores} a wider space, but would be less refined.
    \\
    Ensemble vote
    & Choose the model where there is most contention among the votes in an ensemble. 
    & Requires the training and execution of multiple models, and we further depend on the quality of multiple models. 
    \\

      \bottomrule[1pt]
    \end{tabular}\hspace*{-1cm}
  \end{table*}

\section{Uncertainty Reduction}

This approach attempts to reduce uncertainty of estimates and decision boundaries. The intuition here is that we pick the data points closest to the decision boundary in order to refine its precision. The data points that are covered with high confidence are considered less important because the model already knows how to handle them - adding more data that is easy to label is less valuable. However, this approach does not take into consideration the fact that the most nefarious data points are those where model is both confident and incorrect.

A possible selection technique is selecting the pairs of sentences in ascending order by the difference in their outputted scores. Alternatively, we could determine which points are critical for finding an ordering among the relevant sentences. Rather than try to learn from a sample of points we instead determine the total ordering.

\section{Model Impact}

Another option would be to choose points based on how much they would change the model. This could be done by summing the norm of all the gradients of the parameters. The idea here is that Some approaches instead test the accuracy on a validation test when adding a given data point \cite{rubens2009output}. This is expensive, as for each point the validation set must also be evaluated.  

\section{Ensemble vote}

Have multiple models, and choose the data points where models disagree. This can be determined by a weighted vote or a majority vote. For models, options include multiple instances of a given model (the most performant on a validation set) trained with a different randomization of batches, different parameters, or a range of different models. Bagging and boosting also has been used \cite{olsson2009literature}. There have been a wide range of different objective functions proposed for this task like Jensen-Shannon divergence \cite{lin1991divergence}, F-complement \cite{ngai2000rule}, and Kullback-Leibler divergence . 

\section{Experiments}

We will compare a uncertainty based approach versus random sampling as a control group. We will do this by replicating the process within our training data. First however, we need a model that works well enough to measure progress.
